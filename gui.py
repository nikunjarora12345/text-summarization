# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'page11.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
import sys
import subprocess

from page22 import OutputDialog


class UiDialog(object):
    def __init__(self):
        self.summary_type = "histogram"
        self.output_window = None

    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(1000, 618)
        self.listView = QtWidgets.QListView(Dialog)
        self.listView.setGeometry(QtCore.QRect(0, 0, 1000, 618))
        font = QtGui.QFont()
        font.setFamily("Microsoft Tai Le")
        self.listView.setFont(font)
        self.listView.setStyleSheet("background-image: url(\"image_background.jpg\")")
        self.listView.setObjectName("listView")
        self.textEdit = QtWidgets.QTextEdit(Dialog)
        self.textEdit.setGeometry(QtCore.QRect(210, 60, 751, 51))
        self.textEdit.setStyleSheet("background-color:rgb(67, 67, 67)")
        self.textEdit.setObjectName("textEdit")
        self.listView_2 = QtWidgets.QListView(Dialog)
        self.listView_2.setGeometry(QtCore.QRect(0, 0, 170, 618))
        self.listView_2.setStyleSheet("background-color:rgb(67, 67, 67)")
        self.listView_2.setObjectName("listView_2")
        self.pushButton = QtWidgets.QPushButton(Dialog)
        self.pushButton.setGeometry(QtCore.QRect(510, 370, 93, 28))
        font = QtGui.QFont()
        font.setFamily("Algerian")
        self.pushButton.setFont(font)
        self.pushButton.setStyleSheet("background-color: rgb(67, 67, 67)")
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(Dialog)
        self.pushButton_2.setGeometry(QtCore.QRect(0, 120, 170, 50))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_3 = QtWidgets.QPushButton(Dialog)
        self.pushButton_3.setGeometry(QtCore.QRect(0, 170, 170, 50))
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_4 = QtWidgets.QPushButton(Dialog)
        self.pushButton_4.setGeometry(QtCore.QRect(0, 220, 170, 50))
        self.pushButton_4.setObjectName("pushButton_4")
        self.listView_3 = QtWidgets.QListView(Dialog)
        self.listView_3.setGeometry(QtCore.QRect(0, 0, 170, 120))
        self.listView_3.setStyleSheet("background-color:rgb(67, 67, 67);\n"
"background-image:url(\"logo.jpg\")\n"
"")
        self.listView_3.setObjectName("listView_3")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(210, 30, 72, 15))
        self.label.setStyleSheet("color: rgb(255, 255, 255);")
        self.label.setObjectName("label")
        self.label_3 = QtWidgets.QLabel(Dialog)
        self.label_3.setGeometry(QtCore.QRect(210, 130, 72, 15))
        self.label_3.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_3.setObjectName("label_3")
        self.textEdit_2 = QtWidgets.QTextEdit(Dialog)
        self.textEdit_2.setGeometry(QtCore.QRect(210, 160, 581, 51))
        self.textEdit_2.setStyleSheet("background-color:rgb(67, 67, 67)")
        self.textEdit_2.setObjectName("textEdit_2")
        self.label_4 = QtWidgets.QLabel(Dialog)
        self.label_4.setGeometry(QtCore.QRect(210, 230, 72, 15))
        self.label_4.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_4.setObjectName("label_4")
        self.textEdit_3 = QtWidgets.QTextEdit(Dialog)
        self.textEdit_3.setGeometry(QtCore.QRect(210, 260, 581, 51))
        self.textEdit_3.setStyleSheet("background-color:rgb(67, 67, 67)")
        self.textEdit_3.setObjectName("textEdit_3")
        self.pushButton_5 = QtWidgets.QPushButton(Dialog)
        self.pushButton_5.setGeometry(QtCore.QRect(830, 170, 93, 28))
        self.pushButton_5.setObjectName("pushButton_5")
        self.pushButton_6 = QtWidgets.QPushButton(Dialog)
        self.pushButton_6.setGeometry(QtCore.QRect(830, 250, 93, 28))
        self.pushButton_6.setObjectName("pushButton_6")

        self.listView.raise_()
        self.listView_2.raise_()
        self.textEdit.raise_()
        self.pushButton.raise_()
        self.pushButton_2.raise_()
        self.pushButton_3.raise_()
        self.pushButton_4.raise_()
        self.listView_3.raise_()
        self.label.raise_()
        self.label_3.raise_()
        self.textEdit_2.raise_()
        self.label_4.raise_()
        self.textEdit_3.raise_()
        self.pushButton_5.raise_()
        self.pushButton_6.raise_()
        self.set_button_color()

        self.retranslateUi(Dialog)
        self.pushButton.clicked.connect(self.open_output_window)
        self.pushButton_2.clicked.connect(self.select_histogram)
        self.pushButton_3.clicked.connect(self.select_rake)
        self.pushButton_4.clicked.connect(self.select_sumy)
        self.pushButton_5.clicked.connect(self.browse_file_text_edit_2)
        self.pushButton_6.clicked.connect(self.browse_file_text_edit_3)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def browse_file_text_edit_2(self):
        options = QtWidgets.QFileDialog.Options()
        file_name, _ = QtWidgets.QFileDialog.getOpenFileName(None, 'Single File', QtCore.QDir.currentPath(), '*.html',
                                                             options=options)
        if file_name:
            self.textEdit_2.setText(file_name)

    def browse_file_text_edit_3(self):
        options = QtWidgets.QFileDialog.Options()
        file_name, _ = QtWidgets.QFileDialog.getOpenFileName(None, 'Single File', QtCore.QDir.currentPath(), '*.txt',
                                                             options=options)
        if file_name:
            self.textEdit_3.setText(file_name)

    def select_histogram(self):
        self.summary_type = "histogram"
        self.set_button_color()

    def select_rake(self):
        self.summary_type = "rake"
        self.set_button_color()

    def select_sumy(self):
        self.summary_type = "sumy"
        self.set_button_color()

    def set_button_color(self):
        if self.summary_type == "histogram":
            self.pushButton_2.setStyleSheet("background-color:rgb(67, 100, 67)")
        else:
            self.pushButton_2.setStyleSheet("background-color:rgb(67, 67, 67)")

        if self.summary_type == "rake":
            self.pushButton_3.setStyleSheet("background-color:rgb(67, 100, 67)")
        else:
            self.pushButton_3.setStyleSheet("background-color:rgb(67, 67, 67)")

        if self.summary_type == "sumy":
            self.pushButton_4.setStyleSheet("background-color:rgb(67, 100, 67)")
        else:
            self.pushButton_4.setStyleSheet("background-color:rgb(67, 67, 67)")

    def open_output_window(self):
        url = self.textEdit.toPlainText()
        file = self.textEdit_2.toPlainText()
        ref = self.textEdit_3.toPlainText()

        if not url and not file:
            return

        command = "python "
        if self.summary_type == "histogram":
            command += "histogram.py"
        elif self.summary_type == "rake":
            command += "rake_tokenizer.py"
        elif self.summary_type == "sumy":
            command += "sumy_summarizer.py"

        if url:
            command += " --url " + url
        elif file:
            command += " --path \"" + file + "\""

        if ref:
            command += " --ref \"" + ref + "\""

        process = subprocess.run(command, shell=True)
        print(process.stdout)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.pushButton.setText(_translate("Dialog", "Summarize"))
        self.pushButton_2.setText(_translate("Dialog", "Histogram"))
        self.pushButton_3.setText(_translate("Dialog", "Rake"))
        self.pushButton_4.setText(_translate("Dialog", "Sumy"))
        self.label.setText(_translate("Dialog", "URL"))
        self.label_3.setText(_translate("Dialog", "HTML file"))
        self.label_4.setText(_translate("Dialog", "Reference"))
        self.pushButton_5.setText(_translate("Dialog", "browse"))
        self.pushButton_6.setText(_translate("Dialog", "browse"))


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = UiDialog()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
