from rake_nltk import Metric, Rake
import argparse
import re
import spacy
import requests

import summarizer

nlp = spacy.load("en_core_web_sm")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--url", help="Enter a remote url", required=False)
    parser.add_argument("--path", help="Enter a local path", required=False)
    parser.add_argument("--ref", help="Enter path to reference summary", required=False)
    args = parser.parse_args()

    if not args.url and not args.path:
        raise Exception("Enter either the url or the path. See -h for help")

    r = Rake(min_length=3, max_length=5, ranking_metric=Metric.WORD_FREQUENCY)

    html = ""

    if args.url:
        html = requests.get(args.url).text
    elif args.path:
        html = open(args.path).read()

    if not html:
        raise Exception("Malformed url or path")

    text = summarizer.sanitize_html(html)
    filtered_text = re.sub('[^A-Za-z]+', ' ', text)
    filtered_text = re.sub(r'\s+', ' ', filtered_text)

    entities = []

    tokens = nlp(filtered_text)

    for ent in tokens.ents:
        if ent.label_ == "PERSON" or ent.label_ == "ORG":
            if ent.text not in entities:
                entities.append(ent.text)

    r.extract_keywords_from_text(filtered_text)
    score = r.get_ranked_phrases()

    final_summary = summarizer.summarize(html, score[0:10], entities)
    print("Summary: " + final_summary)
    if args.ref:
        print("Rouge 2 score: ", summarizer.get_rouge_metric(final_summary, args.ref, 2))
        print("Rouge 3 score: ", summarizer.get_rouge_metric(final_summary, args.ref, 3))


if __name__ == "__main__":
    main()
