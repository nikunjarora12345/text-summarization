import spacy
import bs4 as bs
import re
import matplotlib.pyplot as plt
import itertools
import requests
import argparse

import summarizer

nlp = spacy.load("en_core_web_sm")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--url", help="Enter a remote url", required=False)
    parser.add_argument("--path", help="Enter a local path", required=False)
    parser.add_argument("--ref", help="Enter path to reference summary", required=False)
    args = parser.parse_args()

    if not args.url and not args.path:
        raise Exception("Enter either the url or the path. See -h for help")

    text = ""

    if args.url:
        text = parse_html(args.url)
    elif args.path:
        text = parse_html(args.path, url=False)

    if not text:
        raise Exception("Malformed url or path")

    entities, tokens = get_tokens_and_entities_from_sentence(text)

    frequency_list = get_word_frequencies(tokens, sort="DESC", slice_end=10)

    html = ""

    if args.url:
        html = requests.get(args.url).text
    elif args.path:
        html = open(args.path).read()

    if not html:
        raise Exception("Malformed url or path")

    final_summary = summarizer.summarize(html, frequency_list.keys(), entities)

    print("Summary: " + final_summary)
    if args.ref:
        print("Rouge 2 score: ", summarizer.get_rouge_metric(final_summary, args.ref, 2))
        print("Rouge 3 score: ", summarizer.get_rouge_metric(final_summary, args.ref, 3))

    # plot the words as histogram
    plt.bar(frequency_list.keys(), frequency_list.values(), 0.5, color='r')
    plt.xticks(rotation=45)
    plt.show()


def parse_html(path, url=True):
    """
    Returns texts in certain html elements as a single text
    :param str path: The url or local path of html file
    :param bool url: Is the path a url
    :return: String combining all texts
    """
    if not url:
        # parse the html file
        data = bs.BeautifulSoup(open(path), "lxml")
    else:
        r = requests.get(path)
        data = bs.BeautifulSoup(r.text, "lxml")

    elements = data.find_all(["title", "header", "h1", "h2", "h3", "h4", "h5", "h6", "b"], text=True)

    text = ""

    for e in elements:
        text += e.text
        text += " "

    return text


def get_tokens_and_entities_from_sentence(text):
    """
    Creates tokens from a sentence after removing numbers, special characters and stop words.
    Also parses all the named entities.
    :param str text: Input sentence
    :return: Entities and Tokens as tuple
    """
    # filter out numbers and special characters
    filtered_text = re.sub('[^A-Za-z]+', ' ', text)
    filtered_text = re.sub(r'\s+', ' ', filtered_text)

    entities = []

    # filter out stop words and spaces
    tokens_lower = nlp(filtered_text.lower())
    tokens = nlp(filtered_text)

    for ent in tokens.ents:
        if ent.label_ == "PERSON" or ent.label_ == "ORG":
            if ent.text not in entities:
                entities.append(ent.text)

    filtered_token = []
    for word in tokens_lower:
        if not word.is_stop and word.text != " ":
            filtered_token.append(word)

    return entities, filtered_token


def get_word_frequencies(tokens, sort="ASC", slice_end=0):
    """
    Get word frequencies from a list of word tokens
    :param list tokens: Input tokens
    :param str sort: Sort the dictionary in ascending ("ASC") or descending ("DESC") order. "ASC" by default.
    :param int slice_end: slice the dictionary upto this index. 0 for returning unsliced dictionary. Defaults to 0.
    :return: Dictionary containing words and their frequencies sorted and sliced to specified amount.
    """
    frequency_list = {}

    # add word to word frequency list
    for word in tokens:
        lower_case = word.text.lower()

        if lower_case in frequency_list.keys():
            frequency_list[lower_case] += 1
        else:
            frequency_list[lower_case] = 1

    # Sort the dictionary in descending order and get the first 10 words
    frequency_list = {k: v for k, v in sorted(frequency_list.items(), key=lambda item: item[1],
                                              reverse=(sort == "DESC"))}
    if slice_end > 0:
        frequency_list = dict(itertools.islice(frequency_list.items(), slice_end))

    return frequency_list


if __name__ == "__main__":
    main()

